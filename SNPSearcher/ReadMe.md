# SNPSearcher

This is the ReadMe of the website SNPSearcher I developped.
This app lets its users navigate through a SNP database, those SNPs are linked to different disease traits.

The home page have 3 main links:
- Phenotype search: the user can search for a certain phenotype in the database and get more information about it
- Phenotype List page: the user will be able to scroll through a list of phenotypes and get more information about it.
- SNP Search form page: the user can search by SNP Rsid or Region informations in the database.

### Prerequisites and Installing

In order to make this app function on your personal computer, follow the following steps:

Install dependencies:

```
pip install -r requirements.txt
```

The database is already filled up with the information needed.

But if you want to fill it by your self, follow these steps:

1. Type the following commands:
```
python3 manage.py makemigrations
```
then
```
python3 manage.py migrate
```

Wait untill the database is charged, it takes a while.

2. Go to the home page and add /urlpaume to it. It will show you a "Browse" button that lets it you upload a different .tsv file with the data inside it.

Done!

If you want to run the server and see the app locally, just type:

```
python3 manage.py runserver
```

## Deployment on the web

The app is available anytime at:
[SNPSearcher](https://morning-lake-27673.herokuapp.com/)

## Built With

* [Django](https://www.djangoproject.com/) - The web framework used
* [Visual Studio Code](https://code.visualstudio.com/) - The IDE used

## Authors

* **Draia-Nicolau Ondina** 
M2 Bio-informatique DLAD 2019-2020 at AMU

## Acknowledgments

* The holy internet navigator
* Stack Overflow
* My dear collegues
* Inspiration
* Coffee, Tea & Alcohol

