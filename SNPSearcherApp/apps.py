from django.apps import AppConfig


class SnpsearcherappConfig(AppConfig):
    name = 'SNPSearcherApp'
