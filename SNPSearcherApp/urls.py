from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name = "home"),
    path('Phenotype_search', views.phenotype_search),
    path('Phenotype_list_page', views.PhenListView.as_view(), name = "phens"),
    path('Phenotype_detail_page/<id>', views.phenotype_detail, name = "phens_detail"),
    path('SNP_search_form_page', views.search_form, name = "search_form"),
    path('SNP_search_results_page/<field>/<id>', views.SNP_search_results, name = "search_results"),
    path('SNP_detail_page/<id>', views.SNP_detail_page, name="snp_detail_page"),
    path('urlpaume', views.load)
]