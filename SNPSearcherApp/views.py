import csv 
import datetime
from django.http import HttpResponse
from django.shortcuts import render, redirect #methode de django.shortcut qui permet de generer un objet HtppResponse apres avoir traite le template
from django.forms import *
from .models import SNP, SNP2Phenotype2Ref, Reference, DISEASE_TRAIT
from .forms import snp_file
from django.views import generic

def home(request): 
    """ Exemple de page non valide au niveau HTML pour que l'exemple soit concis """
    return render(request, 'SNPSearcherApp/home.html' #ca sera mon squelette
    )

def phenotype_search(request):
        template = 'SNPSearcherApp/phenotype_search.html'
        phen_list = []
        for phen in DISEASE_TRAIT.objects.all():
            phen_list.append(phen.Disease_Trait)
        if request.method == 'POST': #methode post pour renvoyer correctement des informations
            value = request.POST.get('phen_name')
            dis = DISEASE_TRAIT.objects.get(Disease_Trait__icontains=value) #ecriture mystique de django, permet de verifier si dans disease_trait contient la valeur donnee
            if type(dis) == list:
                return redirect('PhenListView.as_view()') #redirige vers la view specifiee
            else:
                return redirect(phenotype_detail, id=dis.id)
        return render(request, template, { 'Phenotypes': phen_list})

class PhenListView(generic.ListView):
    model = DISEASE_TRAIT
    context_object_name = 'phen_list'   # your own name for the list as a template variable
    template_name = 'SNPSearcherApp/phenotype_list_page.html'  # Specify your own template name/location

def phenotype_detail(request, id):
    dis = DISEASE_TRAIT.objects.get(id=id)
    ref_list = []
    pid_list = []
    snp_list = []
    for S2R in SNP2Phenotype2Ref.objects.all().filter(Disease_Trait_Id=dis.id):
        if S2R.Reference_Id.Study not in ref_list:
            ref_list.append(S2R.Reference_Id.Study)
        if S2R.Reference_Id.Pubmed_id not in pid_list:
            pid_list.append(S2R.Reference_Id.Pubmed_id)
        if S2R.SNP_Id.Rsid not in snp_list:
            snp_list.append(S2R.SNP_Id.Rsid)
    return render(request, 'SNPSearcherApp/phenotype_detail_page.html', 
                {'Disease_Trait': dis.Disease_Trait, 'Refs': ref_list, 'Pid' : pid_list, 'Rsid' : snp_list })

def search_form(request):
    """Page du formulaire de recherche de SNPs"""
    template = 'SNPSearcherApp/snp_search_form_page.html'
    reg = []
    rid = []
    if request.method == 'POST':
        value1 = request.POST.get('rsid')
        value2 = request.POST.get('region')
        search = request.POST.get('name')
        if value1 == "rsid":
            return redirect(SNP_search_results, field='rsid', id=search )
        elif value2 == "region":
            return redirect(SNP_search_results,field='region', id=search)
    return render(request,template)

def SNP_search_results(request, field, id):
    l = []
    if field == 'rsid':
        res = SNP.objects.filter(Rsid__icontains=id)
    else:
        res = SNP.objects.filter(Region__icontains=id)
    # print(res)
    for i, snp in enumerate(res):
        # print(i)
        # S2P2R = SNP2Phenotype2Ref.objects.filter(SNP_Id=SNP.objects.get(id=snp.id))[i:i+1].get()
        S2P2R = SNP2Phenotype2Ref.objects.filter(SNP_Id=snp.id)
        S2P2R = S2P2R[:1].get() if S2P2R.count() > 1 else S2P2R.get()
        c = S2P2R.SNP_Id.Chr_Id
        r = S2P2R.SNP_Id.Rsid
        p = S2P2R.Disease_Trait_Id.Disease_Trait
        cx = S2P2R.SNP_Id.Context
        sa = S2P2R.SNP_Id.Strongest_SNP_Risk_Allele
        pv = S2P2R.P_value
        pm = S2P2R.P_value_mlog
        reg = S2P2R.SNP_Id.Region
        l.append({'Region': reg, 'Chr_Id': c, 'Disease_Trait': p, 'Rsid': r, 'Cxt': cx.replace('_variant',''), 'SA': sa, 'PV': pv, 'PM': pm})
    print(l)
    return render(request, 'SNPSearcherApp/SNP_search_results_page.html', {'Items': l, 'ID': S2P2R.id, 'Id' : S2P2R.Disease_Trait_Id.id} )

def SNP_detail_page(request, id):
    s = SNP2Phenotype2Ref.objects.get(id=id)
    for snp in SNP2Phenotype2Ref.objects.filter(id=id):
        c = snp.SNP_Id.Chr_Id
        r = snp.SNP_Id.Rsid
        p = snp.Disease_Trait_Id.Disease_Trait
        cx = snp.SNP_Id.Context
        sa = snp.SNP_Id.Strongest_SNP_Risk_Allele
        pv = snp.P_value
        pm = snp.P_value_mlog
        reg = snp.SNP_Id.Region
    for ref in SNP2Phenotype2Ref.objects.filter(id=id):
        stu = ref.Reference_Id.Study
        j = ref.Reference_Id.Journal
        aut = ref.Reference_Id.First_Author
        date = ref.Reference_Id.Date
        sz = ref.Reference_Id.Sample_Size
    return render(request, 'SNPSearcherApp/SNP_detail_page.html',
        {'ID': snp.Disease_Trait_Id.id, 'Rsid':r, 'Date': date, 'Sample_size' : sz, 'Chr_Id': c, 'Journal' : j, 'Author': aut, 'Study': stu, 'Region': r, 'Phenotype': p, 'Context': cx, 'Strongest_Allele': sa, 'P_value': pv, 'P_valueMlog': pm  })

def load(request): #script qui lit le .tsv
    form = snp_file(request.POST or None, request.FILES)

    if form.is_valid():
        first = True
        file = form.files['tsv'].open(mode="rb")
        data = str(file.read(), 'utf-8')
        for line in data.split('\n'):
            line = line.split('\t')
            if first == True or len(line) < 18:  # passer le heather ou eviter une erreur a la fin
                first = False
                continue
            # obtient les annees, les mois et les jours: avoir la date plus tard
            y, m, d = [int(d) for d in line[2].split('-')]
            # remplace les NR par des none afin d'avoir des n/a
            if line[15] == 'NR':
                line[15] = None
            # remplace les string vides par des Nones afin de remplir la db avec des n/a
            for i, col in enumerate(line): 
                if col == '':
                    line[i] = None
            # Creation du SNP dans la db si ca n'existe pas
            SNP.objects.get_or_create(Chr_Id=line[9], Chr_Pos=line[10], Rsid=line[12], Region=line[8],
                        Context=line[14], Strongest_SNP_Risk_Allele=line[11][-1], 
                        Risk_Allele_Frequency=line[15])
            # Creation du DISEASE_TRAIT dans la db 
            DISEASE_TRAIT.objects.get_or_create(Disease_Trait=line[6])
            # Creation de la Reference dans la db 
            Reference.objects.get_or_create(Pubmed_id=line[0], Journal=line[3], First_Author=line[1], 
                Study=line[5], Sample_Size=line[7], Date=datetime.date(y, m, d))
            # Creation du SNP2Phenotype2Ref dans la db 
            SNP2Phenotype2Ref.objects.get_or_create(
                SNP_Id = SNP.objects.get(Chr_Id=line[9], Chr_Pos=line[10], Rsid=line[12], Region=line[8],
                        Context=line[14], Strongest_SNP_Risk_Allele=line[11][-1], 
                        Risk_Allele_Frequency=line[15]),
                Disease_Trait_Id=DISEASE_TRAIT.objects.get(Disease_Trait=line[6]),
                Reference_Id=Reference.objects.get(Pubmed_id=line[0], Journal=line[3], First_Author=line[1], 
                        Study=line[5], Sample_Size=line[7], Date=datetime.date(y, m, d)),
                P_value=float(line[16]), 
                P_value_mlog=float(line[17]))
    
    return render(request, 'SNPSearcherApp/load.html', {'form': form})

