from import_export import resources
from .models import SNP2Phenotype2Ref, SNP, Reference, DISEASE_TRAIT

class SNP2PhenResource(resources.ModelResource):
    class Meta:
        model = SNP2Phenotype2Ref

class SNPResource(resources.ModelResource):
    class Meta:
        model = SNP

class ReferenceResource(resources.ModelResource):
    class Meta:
        model = Reference

class DiseaseTraitResource(resources.ModelResource):
    class Meta:
        model = DISEASE_TRAIT