from django.db import models

# Create your models here.
# Structure de la database: relier a un script ou une view qui va inserer dans chaque zone les infos qu'il faut 

#Les modèles qui, comme expliqué dans la première partie,
#sont des interfaces permettant plus simplement d'accéder à des données dans une base de données et de les mettre à jour.

#Un modèle s'écrit sous la forme d'une classe et représente une table dans la base de données
# chaque catégorie commence par class 
# Each model is a Python class that subclasses django.db.models.Model.
# Each attribute of the model represents a database field.
# With all of this, Django gives you an automatically-generated database-access API; see Making queries.

#A mettre:
class Reference(models.Model): 

    id = models.IntegerField(primary_key=True)
    Pubmed_id = models.CharField(max_length= 10) #obligatoire de definir une maxlength
    First_Author = models.CharField( max_length= 30)
    Date = models.DateField()
    Journal = models.CharField(max_length = 30)
    Link = models.CharField(max_length= 50) 
    Study =  models.CharField(max_length= 50)
    Sample_Size = models.CharField(max_length= 50)

class DISEASE_TRAIT(models.Model):

    id = models.IntegerField(primary_key=True)
    Disease_Trait = models.CharField(max_length= 20)

class SNP(models.Model):

    id = models.IntegerField(primary_key=True)
    # SNP_Id_Current = models.IntegerField()
    Chr_Id = models.CharField(max_length= 5)
    Chr_Pos = models.CharField(max_length=30)
    Region = models.CharField(max_length= 15)
    Strongest_SNP_Risk_Allele = models.CharField(max_length= 15)
    Rsid = models.CharField(max_length = 15)
    Risk_Allele_Frequency = models.CharField(max_length=30)
    Context = models.CharField(max_length= 15)

class SNP2Phenotype2Ref(models.Model):

    id = models.IntegerField(primary_key=True)
    SNP_Id = models.ForeignKey(SNP, on_delete = models.PROTECT)
    Disease_Trait_Id = models.ForeignKey(DISEASE_TRAIT, on_delete = models.PROTECT) #appeler disease trait d'une maniere ou d'une autre
    Reference_Id = models.ForeignKey(Reference, on_delete = models.PROTECT)
    P_value = models.FloatField()
    P_value_mlog = models.FloatField()





