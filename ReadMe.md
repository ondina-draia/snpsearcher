# SNPSearcher

This is the ReadMe of the website SNPSearcher I developped.
This app lets its users navigate through a SNP database, those SNPs are linked to different disease traits.

The home page have 3 main links:
- Phenotype search: the user can search for a certain phenotype in the database and get more information about it.
- Phenotype List page: the user will be able to scroll through a list of phenotypes and get more information about it.
    When arriving on the Phenotype detail page once you have chosen a phenotype, it is possible to click on the study link and it will get you to the associated pubmed page.
- SNP Search form page: the user can search by SNP Rsid or Region informations in the database.If you enter incomplete rsid name or region name, the page that will appear will show you a list of
    possible results.
    When arriving at the search results table, you can click on the rsid to get more information. Or you can also click on the phenotype/disease trait associated to get more information.
- The title SNPSearcher gets you to the home page you click on it.

### Prerequisites and Installing

In order to make this app function on your personal computer, follow the following steps:

You can clone this git repository easily with this command:

```
git clone https://gitlab.com/ondina-draia/snpsearcher.git

```

Install dependencies:

Go into the snpsearcher file:
Example:
```
cd snpsearcher
```
And type those commands:
```
sudo apt install libpq-dev python3-dev

```
and then:
```
pip3 install -r requirements.txt
```

If you want to run the server and see the app locally, just type:

```
python3 manage.py runserver
```
and then follow the link to the developpement server with your favorite web browser. Enjoy!

------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------

Information:

The database is already filled up with all the information needed.

But if you want to fill it by yourself, follow these steps:

1. Type the following commands:
```
python3 manage.py makemigrations
```
then
```
python3 manage.py migrate
```

Wait untill the database is charged, it takes a while.

2. Go to the home page and add /urlpaume to it. It will show you a "Browse" button that lets it you upload a different .tsv file with the data inside it.

Done!



## Deployment on the web

The app is available anytime at:
[SNPSearcher](https://morning-lake-27673.herokuapp.com/)

## Built With

* [Django](https://www.djangoproject.com/) - The web framework used
* [Visual Studio Code](https://code.visualstudio.com/) - The IDE used

## Authors

* **Draia-Nicolau Ondina** 
M2 Bio-informatique DLAD 2019-2020 at AMU

## Acknowledgments

* The holy internet navigator
* Stack Overflow
* My dear collegues
* Inspiration
* Coffee, Tea & Alcohol

